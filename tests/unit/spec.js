describe('my application', function(){
    describe('my controllers', function(){
        beforeEach(module('productList'));

        describe('my productlistcontroller', function(){
            var scope;
            beforeEach(inject(function($rootScope, $controller){
                scope = $rootScope.$new();
                $controller('ProductListController as plc', {$scope: scope})
            }));

            //it('should instantiate default data', function(){
            //    expect(scope.plc.products.length).toBe(5);
            //});
        });    
    });
   
    
    describe('my API service', function(){
        beforeEach(module('dataServices'));
        
        var ProductService;
        beforeEach(inject(function(_ProductService_){
            ProductService = _ProductService_;
        }));
        
        it('should return the products', function(){
            ProductService.getProducts().then(function(data){
                expect(data.length).toBe(5);
            });
        })
    });
    
});
