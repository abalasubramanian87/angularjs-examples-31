angular.module('myApp', 
    [
        'ngRoute',
        'productList', 
        'orderForm', 
        'dataServices'])
    .config(function($routeProvider){
        $routeProvider.when('/', {
            "templateUrl": "app/productList/productList.html",
            "controller": "ProductListController",
            "controllerAs": "plc",
            "resolve": {
                "productList": function(ProductService){
                    return ProductService.getProducts();
                }
            }
        }).when('/about', {
            "template": '<div>Hello WOrld</div>'
        }).when('/bigDataCharts',{
            "template": '<div>Some Data {{ bdc.data }}</div>',
            "controller": function(bigData){
                this.data = bigData
            },
            "controllerAs": "bdc",
            "resolve": {
                "bigData": function($timeout){
                    return $timeout(function(){
                        return 42;
                    }, 3000);
                }
            }
        });
    });
