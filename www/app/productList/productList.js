angular.module('productList', [])

.controller('ProductListController', ProductListController);
function ProductListController($scope, $location, UserService, productList, ProductService, $q){
    var plc = this;
    
    $scope.$watch(function(){
        return plc.search;
    }, function(n, o){
        if(plc.search === 'Hello'){
            plc.updateFunction();
        }
    });
    plc.updateFunction = function(){
        console.log('updateFunction called');
    };
    
    var deferred1 = $q.defer();
    var deferred2 = $q.defer();
    var deferred3 = $q.defer();
    
    var promise1 = deferred1.promise;
    var promise2 = deferred2.promise;
    var promise3 = deferred3.promise;
    
    promise1.then(function(data){
        console.log(data);
    });
    
    $q.all([promise1, promise2, promise3]).then(function(){
        console.log('all buttons pressed');
    });
    
    
    plc.button2Click = function(){
        deferred2.resolve('button 2 pressed');
    };
    plc.button3Click = function(){
        deferred3.resolve('button 3 pressed');
    };
    plc.login = function(){
        
        deferred1.resolve('button 1 pressed');
        
        
       //UserService.login({"password": 12345}).then(function(userInfo){
       //    console.log('login called again and looking at the results of original promise')
       //    if(userInfo.status === "success"){
       //        plc.loginMessage = "User logged in Successfully";
       //    }else{
       //        plc.loginMessage = "User logged in failed"
       //    }
       //}, function(err){
       //    plc.loginMessage = "user login failed/server error";
       //});
       //ProductService.UpdateData(); 
    };
    
    plc.products = productList;
    plc.loading = false;
    plc.goToBigDataPage = function(){
        plc.loading = true;
        $location.url("/bigDataCharts");
        console.log('calling');
    };
    
    ProductService.getFirstProduct().then(function(firstElement){
        plc.firstProduct = firstElement;
    })
}
