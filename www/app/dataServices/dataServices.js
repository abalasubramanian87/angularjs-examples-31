angular.module('dataServices', [])
    .service('ProductService', ProductService);

function ProductService($http, $q, UserService, $timeout){
    var productService = this;
    this.getProducts = function(){
            return $http.get('data/products.json').then(function (res) {
                return res.data;
            });
    };
    
    this.getProducts2 = function(){
        return $http.get('data/products2.json').then(function(res){
            return res.data;
        });
    };
    
    this.getProducts3 = function(){
        return $http.get('data/products3.json').then(function(res){
            return res.data;
        });
    };
    
    this.getCombination = function(){
        var promise1 = productService.getProducts();
        var promise2 = productService.getProducts2();
        var promise3 = productService.getProducts3();
        //var q1all = $q.all([promise1, promise2, promise3]).then(function (results){
        //    console.log(results);
        //});
        
        var q2all = $q.all([promise2, promise3]).then(function(results){
            promise1.then(function(){
                // all three
            })
            //just the two
        })
        
    };
    
    productService.getCombination();
    
    this.getFirstProduct = function(){
        return productService.getProducts().then(function(data){
            return data[0];
        })
    };
    
    this.UpdateData = function(user){
        UserService.login(user).then(function(userInfo){
            if(userInfo.role === "admin"){
                console.log('fetching admin data');
            }else{
                console.log('fetching user data');
            }
        }).catch(function(err){
            console.log('return default data')
        })
    };
    
    
};
