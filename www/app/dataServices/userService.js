angular.module('dataServices')
.service('UserService', UserService);

function UserService($http){
    var loginPromise;
    this.login = function(user) {
        if(!loginPromise){
            loginPromise = $http.get('data/xuser.json').then(function(res){
                if(res.data.password === user.password){
                    return {
                        "status": "success",
                        "role": res.data.role
                    }
                }
                
                return {
                    "status": "failed"
                }
            });
        }
        return loginPromise;
    };
}
