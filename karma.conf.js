module.exports = function(config) {
  config.set({

    basePath: '',
    frameworks: ['jasmine'],
    files: [
        'www/lib/jquery/dist/jquery.js',
        'www/lib/angular/angular.js',
        'node_modules/angular-mocks/angular-mocks.js',
        
        'www/app/app.js',
        'www/app/dataServices/dataServices.js',
        'www/app/dataServices/userService.js',
        'www/app/orderForm/orderForm.js',
        'www/app/productList/productList.js',
        
        'tests/unit/**/*.js'
    ],
    exclude: [
    ],
    preprocessors: {
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false
  })
}
